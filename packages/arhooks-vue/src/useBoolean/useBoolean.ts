/*
 * @author: Archy
 * @Date: 2022-05-05 13:49:36
 * @LastEditors: Archy
 * @LastEditTime: 2022-05-18 15:03:13
 * @FilePath: \arhooks\packages\arhooks-vue\src\useBoolean\useBoolean.ts
 * @description: 对boolean类型的state
 */
import { ref, readonly } from 'vue'
import type { Ref } from 'vue'

export type setBooleanActions = {
  toggle: () => void
  set: (value: boolean) => void
  setTrue: () => void
  setFalse: () => void
}

/**
 * @description: 
 * @param {boolean} initial 初始化
 * @return {[Ref<boolean>, setBooleanActions]}
 */
export const useBoolean = (initial?: boolean): [Ref<boolean>, setBooleanActions] => {
  const state = ref<boolean>(initial || false)

  const set = (value: boolean) => {
    state.value = value
  }
  const toggle = () => {
    set(!state.value)
  }
  const setTrue = () => {
    set(true)
  }
  const setFalse = () => {
    set(false)
  }

  return [state, { toggle, set, setTrue, setFalse }]
}