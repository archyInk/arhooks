/*
 * @author: Archy
 * @Date: 2022-05-05 11:10:17
 * @LastEditors: Archy
 * @LastEditTime: 2022-06-01 11:18:35
 * @FilePath: \arhooks\packages\arhooks-vue\src\useCreateElement\useCreateElement.ts
 * @description: 创建Element的钩子函数
 */

import { isRef, unref, watch } from "vue"
import type { Ref } from 'vue'

export type ParentType = Document | HTMLElement | Element | undefined
/**
 * @description: 创建Element的钩子函数
 * @param {keyof HTMLElementTagNameMap} tagName 指定要创建元素类型的字符串,创建元素时的 nodeName 使用 tagName 的值为初始化，该方法不允许使用限定名称(如:"html:a")
 * @param {Partial<HTMLElementTagNameMap[typeof tagName]>} props 属性对象
 * @param {ElementTarget | Ref<ElementTarget | undefined>} parent
 * @param {ElementCreationOptions} options 一个可选的参数 ElementCreationOptions 是包含一个属性名为 is 的对象，该对象的值是用 customElements.define() 方法定义过的一个自定义元素的标签名。
 * @return {HTMLElement}
 */

export const useCreateElement = <K extends keyof HTMLElementTagNameMap>
  (tagName: K,
    props?: Partial<HTMLElementTagNameMap[typeof tagName]>,
    parent?: ParentType | Ref<ParentType | undefined>,
    options?: ElementCreationOptions): HTMLElementTagNameMap[K] => {
  const element = document.createElement(tagName, options)
  if (props) {
    const propsKeys = props && Object.keys(props) as (keyof HTMLElementTagNameMap[typeof tagName])[]
    for (let prop of propsKeys) {
      element[prop] = props[prop] as HTMLElementTagNameMap[K][keyof HTMLElementTagNameMap[K]]
    }
  }
  
  const p = unref(parent)
  if (p) {
    p!.appendChild(element)
  }

  (isRef(parent) && !unref(parent)) && watch(parent, (_el) => {
    const el = unref(_el)
    if (!el) return
    _el?.appendChild(element)
  }
  )

  return element
}