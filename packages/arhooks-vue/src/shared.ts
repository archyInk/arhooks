/*
 * @author: Archy
 * @Date: 2022-05-06 10:35:26
 * @LastEditors: Archy
 * @LastEditTime: 2022-06-01 11:06:53
 * @FilePath: \arhooks\packages\arhooks-vue\src\shared.ts
 * @description: 
 */
export type ElementTarget = Window | Document | Element | HTMLElement