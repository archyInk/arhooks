import { onBeforeUnmount, onDeactivated, onMounted, watch } from "vue"
import useState from "../useState"

/*
 * @author: Archy
 * @Date: 2022-06-01 11:30:25
 * @LastEditors: Archy
 * @LastEditTime: 2022-06-01 13:38:42
 * @FilePath: \arhooks\packages\arhooks-vue\src\useTitle\useTitle.ts
 * @description: 
 */
import { Ref } from 'vue'
import { SetStateAction } from "../useState/useState"
export const useTitle = (title = ''): { title: Ref<string>, setTitle: (next: SetStateAction<string>) => void } => {
  const [headline, setHeadline] = useState<string>(title)
  const tenet = document.title

  const setTitle = () => { document.title = headline.value }
  const resetTitle = () => { document.title = tenet }
  onMounted(setTitle)

  watch(headline, setTitle)

  onBeforeUnmount(resetTitle)
  onDeactivated(resetTitle)

  return { title: headline, setTitle: setHeadline }
}