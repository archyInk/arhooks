<!--
 * @author: Archy
 * @Date: 2022-06-01 11:30:31
 * @LastEditors: Archy
 * @LastEditTime: 2022-06-01 13:39:55
 * @FilePath: \arhooks\packages\arhooks-vue\src\useTitle\README.md
 * @description: 
-->
# arhooks-vue/useTitle

修改文档标题，unmount之后会恢复上次文档标题

# 使用方式

```javascript
import {useTitle} from 'arhooks-vue'

const {title,setTitle} = useTitle('标题')

setTitle('标题') 

```

# 类型

```javascript
export declare type SetStateAction<T> = T | ((prevState: T) => T);
export declare const useTitle: (title?: string) => {
    title: Ref<string>;
    setTitle: (next: SetStateAction<string>) => void;
}
```

