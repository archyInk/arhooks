/*
 * @author: Archy
 * @Date: 2022-03-22 11:52:25
 * @LastEditors: Archy
 * @LastEditTime: 2022-06-01 13:39:32
 * @FilePath: \arhooks\vue\components\test.tsx
 * @description: 
 */
import { defineComponent } from 'vue';
import { useTitle } from '../../packages/arhooks-vue/src'

export default defineComponent({
  name: '',
  setup() {
    const { title, setTitle } = useTitle('测试')
    setTimeout(() => {
      setTitle('测试后')
    }, 5000)
    return () => {
      return <div>标题为{title.value}</div>
    }
  }
})